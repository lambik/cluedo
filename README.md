# README #

A set of players for the game of Cluedo (Clue in the US).

### What is this repository for? ###

* I wanted to create a javascript implementation of a Clue(do) player

### How do I get set up? ###

* adapt index.htm to your needs (to select the player and the types of cards)
* run index.htm in your browser, and watch how the computer solves the case

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin