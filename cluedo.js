function generateNewGame(items, players, AIs) {
	var ret = gameObject();
	
	// reset data
	ret.items = items;
	for (var pl = 0; pl < players.length; ++pl) {
		ret.addPlayer(pl, AIs[pl], items, players);
	}
	
	// take random card from each category and create murder
	for (var itemtype = 0; itemtype < items.length; ++itemtype) {
		ret.murderer[itemtype] = Math.floor(Math.random() * items[itemtype].length);
	}
	
	// create new deck of cards (concat remaining cards + shuffle)
	var allcardsindices = [];
	var counter = 0;
	for (var itemtype = 0; itemtype < items.length; ++itemtype) {
		for (var item = 0; item < items[itemtype].length; ++item) {
			if (item != ret.murderer[itemtype]) {
				allcardsindices.push(counter);
			}
			++counter;
		}
	}
	
	//+ Jonas Raoni Soares Silva
	//@ http://jsfromhell.com/array/shuffle [rev. #1]
	var shuffle = function(v){
		for(var j, x, i = v.length; i; j = parseInt(Math.random() * i), x = v[--i], v[i] = v[j], v[j] = x);
		return v;
	};
	
	allcardsindices = shuffle(allcardsindices);
	
	// distribute deck among the players, by giving each a card until no more cards are available. This way the fairest spread is achieved.
	// for each card in deck
	var player = 0;
	for (counter = 0; counter < allcardsindices.length; ++counter) {
		// for each player
		// give player this card
		var thecard = null;
		var thetype = null;
		var smallcounter = 0;
		for (var itemtype = 0; itemtype < items.length; ++itemtype) {
			for (var item = 0; item < items[itemtype].length; ++item) {
				if (allcardsindices[counter] == smallcounter) {
					thetype = itemtype;
					thecard = item;
				}
				++smallcounter;
			}
		}
		ret.players[player].addCard(thetype, thecard);
		
		// next card for next player
		player = (player + 1) % players.length;
	}
	
	for (var pl = 0; pl < players.length; ++pl) {
		ret.players[pl].ready();
	}
	
	return ret;
}

function gameObject() {
	return {
		players: [],
		items: [],
		murderer: [],
		
		addPlayer: function(playerid, type, items, players) {
			var pl = null;
			if (type == 'random') {
				pl = getRandomPlayer(playerid);
			}
			else if (type == 'dumb') {
				pl = getDumbPlayer(playerid);
			}
			else if (type == 'normal') {
				pl = getNormalPlayer(playerid);
			}
			else if (type == 'advanced') {
				pl = getAdvancedPlayer(playerid);
			}
			if (pl != null) {
				pl.init(items, players);
				this.players.push(pl);
			}
		}
		
	};
}

function getRandomPlayer(playerindex) {
	// plays totally at random: takes any set of cards from the items and asks around
	// remembers the cards players show him, to make an accusation
	// when a player wants to know what cards this one has, return selection is made at random when multiple are possible
	// does nothing with information gained from the rest of the table and how the others players interact
	return {
		// my player id
		playerid: playerindex,
		
		// my cards
		cards: [],
		
		// whether the player is still in the game or not
		isactive: true,
		
		// whether the player has won the game or not
		winner: false,
		
		// matrix for holding what we already know about the game
		guesstable: [],
		
		// hold all cards in the game, for reference and such
		items: [],
		
		// holds all players in the game, for reference and such
		players: [],
		
		init: function(items, players) {
			for (var i = 0; i < items.length; ++i) {
				this.cards[i] = [];
				
				// copy every card in the items list, but give it a value of null
				// that way we can change the value to a player when we know he has a card like this
				this.guesstable[i] = [];
				for (var j = 0; j < items[i].length; ++j) {
					this.guesstable[i][j] = null;
				}
			}
			this.items = items;
			this.players = players;
		},
		
		// add a card to the array of cards this player has
		// param: itemtype, index of the card
		addCard: function(itemtype, card) {
			this.cards[itemtype].push(card);
			
			// mark card as my deck
			this.guesstable[itemtype][card] = -1; // ideally current player index, but we don't know
		},
		
		// called when the player is fully initialised
		ready: function() {
		},
		
		// generate a new suspect to ask the table
		// output: [index of card, index of card, index of card]
		makeGuess: function() {
			var ret = [];
			// take a completely random card for each itemtype and use it as a guess
			for (var itemtype = 0; itemtype < this.cards.length; ++itemtype) {
				ret.push(Math.floor(Math.random() * this.items[itemtype].length));
			}
			return ret;
		},
		
		// generate an accusation to try to attempt and win the game. return [] when not at this time
		makeAccusation: function() {
			var ret = [];
			var canmakeaccusation = true;
			// check to see if guesstable is completely filled, except for a 'null' value exactly once, per itemtype
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				var typeitem = null;
				for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
					if (this.guesstable[itemtype][item] == null) {
						if (typeitem == null) {
							typeitem = item;
						}
						else {
							canmakeaccusation = false;
						}
					}
				}
				ret[itemtype] = typeitem;
			}
			
			if (!canmakeaccusation) {
				ret = [];
			}
			return ret;
		},
		
		// check if you have a suspected item in my list
		// parameter: [ index of the card the player wants to check, index of another card, index of another card ]
		// return which card you want to show the player if you have at least one
		// return: [ null, index of the card i want to show, null ] (null = card type i don't want to show/have)
		checkGuess: function(guess) {
			var ret = [];
			
			// look in all my cards if i have one or more of the requested cards
			for (var itemtype = 0; itemtype < this.cards.length; ++itemtype) {
				// i can have several cards of the same itemtype
				for (var mycard = 0; mycard < this.cards[itemtype].length; ++mycard) {
					if (this.cards[itemtype][mycard] == guess[itemtype]) {
						// we have this card, now create a mini array with all types, and add the index of this card at the right spot
						var minilist = [];
						for (var it = 0; it < this.cards.length; ++it) {
							if (it == itemtype) {
								minilist[it] = guess[itemtype];
							}
							else {
								minilist[it] = null;
							}
						}
						
						ret.push(minilist);
					}
				}
			}
			// return a random card from the set of found cards
			if (ret.length > 0) {
				return ret[Math.floor(Math.random() * ret.length)];
			}
			else {
				return [];
			}
		},
		
		// function called when a player reacts to the suspect proposed by a player who is not me
		updatePlayerHasOne: function (askerplayer, playerwhohas, move) {
		},
		
		// function called when a player does not react to a suspect proposed by a player (who could be me)
		updatePlayerHasNot: function (askerplayer, playerwhohasnot, move) {
		},
		
		// function called when a player reacts to a suspect proposed by me
		updatePlayerHas: function (player, answer) {
			for (var itemtype = 0; itemtype < answer.length; ++itemtype) {
				if (answer[itemtype] != null) {
					this.guesstable[itemtype][answer[itemtype]] = player;
				}
			}
			
		},
		
		printGuessTable: function() {
			var tempacc = [];
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				tempacc.push(itemtype + " - " + this.guesstable[itemtype]);
			}
			return tempacc;
		}
	};
}




function getDumbPlayer(playerindex) {
	// dumb player plays a little better than at random: asks about items he doesn't already know
	// other: same as random player
	return {
		// my player id
		playerid: playerindex,
		
		// my cards
		cards: [],
		
		// whether the player is still in the game or not
		isactive: true,
		
		// whether the player has won the game or not
		winner: false,
		
		// matrix for holding what we already know about the game
		// this table is filled in differently from the random player:
		// [itemtype][item] = -1, 0, 1
		// -1: my own card
		//  0: never asked someone (== default)
		//  1: asked and got response
		// algorithm: ask each player the first card (per itemtype) that has value == 0, until you get the result. If you get the notice "nobody has card that matches" then you can make the accusation
		guesstable: [],
		
		// a table to store which player responded to our current guess
		playertable: [],
		
		// hold all cards in the game, for reference and such
		items: [],
		
		// holds all players in the game, for reference and such
		players: [],
		
		init: function(items, players) {
			for (var i = 0; i < items.length; ++i) {
				this.cards[i] = [];
				
				// copy every card in the items list, but give it a default value
				// that way we can change the value to 1 when we know a player has a card like this
				this.guesstable[i] = [];
				for (var j = 0; j < items[i].length; ++j) {
					this.guesstable[i][j] = 0;
				}
			}
			this.items = items;
			this.players = players;
		},
		
		// add a card to the array of cards this player has
		// param: itemtype, index of the card
		addCard: function(itemtype, card) {
			this.cards[itemtype].push(card);
			
			// mark card as my deck
			this.guesstable[itemtype][card] = -1;
		},
		
		// called when the player is fully initialised
		ready: function() {
		},
		
		// generate a new suspect to ask the table
		// output: [index of card, index of card, index of card]
		makeGuess: function() {
			var ret = [];
			// take the first unknown-player card for each itemtype and use it as a guess (== run linearly through the array)
			// this implies that if a card is the murderer card, the script will always use that as a guess
			// this implies a change in the makeAccusation code, as it will no longer ever find a single null value per itemtype
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
					if (this.guesstable[itemtype][item] == 0) {
						ret.push(item);
						break;
					}
				}
			}
			
			// prepare the playertable
			for (var pl = 0; pl < players.length; ++pl) {
				if (pl == this.playerid) {
					this.playertable[pl] = 0; // 0 is code for "does not have card"
				}
				else {
					this.playertable[pl] = 1; // 1 is code for "has a possibility of replying"
				}
			}
			
			return ret;
		},
		
		// generate an accusation to try to attempt and win the game. return [] when not at this time
		makeAccusation: function() {
			var ret = [];
			var canmakeaccusation = true;
			// check to see if each player responded to our guess
			// if they all haven't, then we can make the accusation
			for (var pl = 0; pl < this.players.length; ++pl) {
				if (this.playertable[pl] == 1) {
					canmakeaccusation = false;
				}
			}
			
			if (canmakeaccusation) {
				// just get last move again
				ret = this.makeGuess();
			}
			
			return ret;
		},
		
		// check if you have a suspected item in my list
		// parameter: [ index of the card the player wants to check, index of another card, index of another card ]
		// return which card you want to show the player if you have at least one
		// return: [ null, index of the card i want to show, null ] (null = card type i don't want to show/have)
		checkGuess: function(guess) {
			var ret = [];
			
			// look in all my cards if i have one or more of the requested cards
			for (var itemtype = 0; itemtype < this.cards.length; ++itemtype) {
				// i can have several cards of the same itemtype
				for (var mycard = 0; mycard < this.cards[itemtype].length; ++mycard) {
					if (this.cards[itemtype][mycard] == guess[itemtype]) {
						// we have this card, now create a mini array with all types, and add the index of this card at the right spot
						var minilist = [];
						for (var it = 0; it < this.cards.length; ++it) {
							if (it == itemtype) {
								minilist[it] = guess[itemtype];
							}
							else {
								minilist[it] = null;
							}
						}
						
						ret.push(minilist);
					}
				}
			}
			// return a random card from the set of found cards
			if (ret.length > 0) {
				return ret[Math.floor(Math.random() * ret.length)];
			}
			else {
				return [];
			}
		},
		
		// function called when a player reacts to the suspect proposed by a player who is not me
		updatePlayerHasOne: function (askerplayer, playerwhohas, move) {
		},
		
		// function called when a player does not react to a suspect proposed by a player (who could be me)
		updatePlayerHasNot: function (askerplayer, playerwhohasnot, move) {
			// in this player AI, we need to mark the player as not having any card of the move, so that we can know if all players have been given a chance to respond to the guess or not
			// when we know no player has responded, the move we just made is the murderer
			this.playertable[playerwhohasnot] = 0;
		},
		
		// function called when a player reacts to a suspect proposed by me
		updatePlayerHas: function (player, answer) {
			for (var itemtype = 0; itemtype < answer.length; ++itemtype) {
				if (answer[itemtype] != null) {
					this.guesstable[itemtype][answer[itemtype]] = 1;
				}
			}
		},
		
		printGuessTable: function() {
			var tempacc = [];
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				tempacc.push(itemtype + " - " + this.guesstable[itemtype]);
			}
			return tempacc;
		}
	};
}




function getNormalPlayer(playerindex) {
	// normal player plays a little better than dumb: looks at how players react to others (basic version)
	// other: same as dumb player
	return {
		// my player id
		playerid: playerindex,
		
		// my cards
		cards: [],
		
		// whether the player is still in the game or not
		isactive: true,
		
		// whether the player has won the game or not
		winner: false,
		
		// matrix for holding what we already know about the game
		// this table is filled in differently from the dumb player:
		// [itemtype][item][player] = '0', '?', '-', 'X' ('dunno', 'might have', 'does not have', 'has')
		// algorithm: ask each player the card (per itemtype) that has the least information (first the ones with '', then with '?'), until you get the result. You can make an accusation when all cards have either a '' or a '-' or a 'X' (no card has a '?') somewhere in the player row.
		guesstable: [],
		
		// hold all cards in the game, for reference and such
		items: [],
		
		// holds all players in the game, for reference and such
		players: [],
		
		init: function(items, players) {
			for (var i = 0; i < items.length; ++i) {
				this.cards[i] = [];
				
				// copy every card in the items list, but give it a default value
				// that way we can change the value to 1 when we know a player has a card like this
				this.guesstable[i] = [];
				for (var j = 0; j < items[i].length; ++j) {
					this.guesstable[i][j] = [];
					for (var p = 0; p < players.length; ++p) {
						this.guesstable[i][j][p] = '0';
					}
				}
			}
			this.items = items;
			this.players = players;
		},
		
		// add a card to the array of cards this player has
		// param: itemtype, index of the card
		addCard: function(itemtype, card) {
			this.cards[itemtype].push(card);
			
			// mark card as my deck
			this.updateGuessTable(this.playerid, itemtype, card, 'X');
		},
		
		// called when the player is fully initialised
		ready: function() {
			// make sure my guesstable knows that i don't have any other cards
			// the check to not-overwrite the 'X' with '-' happens in the updateGuessTable method
			for (var itemtype = 0; itemtype < this.items.length; ++itemtype) {
				for (var item = 0; item < this.items[itemtype].length; ++item) {
					this.updateGuessTable(this.playerid, itemtype, item, '-');
				}
			}
		},
		
		// internal function to update the guess table with new information
		updateGuessTable: function(player, itemtype, card, newvalue) {
			var oldval = this.guesstable[itemtype][card][player];
			
			// update for the player in parameter
			if (oldval == '0' || oldval == '?') {
				this.guesstable[itemtype][card][player] = newvalue;
			}
			// once a value of '-' or 'X' has been given, it cannot be changed
			
			// update what I know about other players
			if (newvalue == 'X') {
				// nobody else can have this card
				for (var pl = 0; pl < this.players.length; ++pl) {
					if (pl != player) {
						this.guesstable[itemtype][card][pl] = '-';
					}
				}
			}
		},
		
		// generate a new suspect to ask the table
		// output: [index of card, index of card, index of card]
		makeGuess: function() {
			var ret = [];
			
			// for each itemtype, find the card with the least known information:
			// see if there is an accusation (all '-'), pick that if you can (see mental note at the bottom)
			// count the card with the most '0'
			// if not found: count the one with the most '?'
			// if not found: is this possible?
			// mental note: in real life, you shouldn't pick the accusation card, as that could give away hints. You should pick the acc card, your own card if you have one for that itemtype, and some card you know the last player in the sequence will have, so you have the most chance of finding out the card you really want to know about
			
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				var mincard = -1;
				var mincount = 0;
				
				// find "accusation"
				for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
					var cardcount = 0;
					for (var pl = 0; pl < this.players.length; ++pl) {
						if (this.guesstable[itemtype][item][pl] == '-') {
							cardcount++;
						}
					}
					if (cardcount == this.players.length) {
						mincard = item;
					}
				}
				
				if (mincard == -1) {
					// find "most '0'"
					for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
						mincount = 0; // reset
						var cardcount = 0;
						for (var pl = 0; pl < this.players.length; ++pl) {
							if (this.guesstable[itemtype][item][pl] == '0') {
								cardcount++;
							}
						}
						if (cardcount > mincount) { // if two cards have the same count, the first one in the search order will be picked. That's okay!
							mincount = cardcount;
							mincard = item;
						}
					}
				}
				
				if (mincard == -1) {
					// find "most '?'"
					mincount = 0; // reset
					for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
						var cardcount = 0;
						for (var pl = 0; pl < this.players.length; ++pl) {
							if (this.guesstable[itemtype][item][pl] == '?') {
								cardcount++;
							}
						}
						if (cardcount > mincount) { // if two cards have the same count, the first one in the search order will be picked. That's okay!
							mincount = cardcount;
							mincard = item;
						}
					}
				}
				
				if (mincard == -1) {
					// is this possible?
				}
				
				ret.push(mincard);
			}
			
			return ret;
		},
		
		// generate an accusation to try to attempt and win the game. return [] when not at this time
		makeAccusation: function() {
			var ret = [];
			var canmakeaccusation = true;
			
			// for each itemtype, see which card nobody has (all '-')
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				var mincount = 0;
				var mincard = -1;
				for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
					var cardcount = 0;
					for (var pl = 0; pl < this.players.length; ++pl) {
						if (this.guesstable[itemtype][item][pl] == '-') {
							cardcount++;
						}
					}
					if (cardcount > mincount) {
						mincount = cardcount;
						mincard = item;
					}
				}
				if (mincount == this.players.length) {
					ret.push(mincard);
				}
				else {
					canmakeaccusation = false;
				}
			}
			
			if (!canmakeaccusation) {
				// clear ret again if we know it's an invalid accusation
				ret = []
			}
			
			return ret;
		},
		
		// check if you have a suspected item in my list
		// parameter: [ index of the card the player wants to check, index of another card, index of another card ]
		// return which card you want to show the player if you have at least one
		// return: [ null, index of the card i want to show, null ] (null = card type i don't want to show/have)
		checkGuess: function(guess) {
			var ret = [];
			
			// look in all my cards if i have one or more of the requested cards
			for (var itemtype = 0; itemtype < this.cards.length; ++itemtype) {
				// i can have several cards of the same itemtype
				for (var mycard = 0; mycard < this.cards[itemtype].length; ++mycard) {
					if (this.cards[itemtype][mycard] == guess[itemtype]) {
						// we have this card, now create a mini array with all types, and add the index of this card at the right spot
						var minilist = [];
						for (var it = 0; it < this.cards.length; ++it) {
							if (it == itemtype) {
								minilist[it] = guess[itemtype];
							}
							else {
								minilist[it] = null;
							}
						}
						
						ret.push(minilist);
					}
				}
			}
			// return a random card from the set of found cards
			if (ret.length > 0) {
				return ret[Math.floor(Math.random() * ret.length)];
			}
			else {
				return [];
			}
		},
		
		// function called when a player reacts to the suspect proposed by a player who is not me
		updatePlayerHasOne: function (askerplayer, playerwhohas, move) {
			for (var itemtype = 0; itemtype < move.length; ++itemtype) {
				this.updateGuessTable(playerwhohas, itemtype, move[itemtype], '?');
			}
		},
		
		// function called when a player does not react to a suspect proposed by a player (who could be me)
		updatePlayerHasNot: function (askerplayer, playerwhohasnot, move) {
			for (var itemtype = 0; itemtype < move.length; ++itemtype) {
				this.updateGuessTable(playerwhohasnot, itemtype, move[itemtype], '-');
			}
		},
		
		// function called when a player reacts to a suspect proposed by me
		updatePlayerHas: function (player, answer) {
			for (var itemtype = 0; itemtype < answer.length; ++itemtype) {
				if (answer[itemtype] != null) {
					this.updateGuessTable(player, itemtype, answer[itemtype], 'X');
				}
			}
		},
		
		printGuessTable: function() {
			var tempacc = [];
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				tempacc.push(itemtype + ":");
				for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
					tempacc.push(item + '> ' + this.guesstable[itemtype][item]);
				}
			}
			return tempacc;
		}
	};
}




function getAdvancedPlayer(playerindex) {
	// advanced player plays a little better than normal: looks at how players react to others (extended version: keeps track of turns)
	// other: same as normal player
	return {
		// my player id
		playerid: playerindex,
		
		// my cards
		cards: [],
		
		// whether the player is still in the game or not
		isactive: true,
		
		// whether the player has won the game or not
		winner: false,
		
		// matrix for holding what we already know about the game
		// this table is filled in differently from the dumb player:
		// [itemtype][item][player][turn] = '0', '?', '-', 'X' ('dunno', 'might have', 'does not have', 'has')
		// algorithm: ask each player the card (per itemtype) that has the least information (first the ones with '', then with '?'), until you get the result. You can make an accusation when all cards have either a '' or a '-' or a 'X' (no card has a '?') somewhere in the player row. In the extended version, more data about the turns is used.
		guesstable: [],
		
		// hold all cards in the game, for reference and such
		items: [],
		
		// holds all players in the game, for reference and such
		players: [],
		
		init: function(items, players) {
			for (var i = 0; i < items.length; ++i) {
				this.cards[i] = [];
				
				// copy every card in the items list, but give it a default value
				// that way we can change the value to 1 when we know a player has a card like this
				this.guesstable[i] = [];
				for (var j = 0; j < items[i].length; ++j) {
					this.guesstable[i][j] = [];
					for (var p = 0; p < players.length; ++p) {
						this.guesstable[i][j][p] = [];
					}
				}
			}
			this.items = items;
			this.players = players;
			this.addTurn(this.playerid);
		},
		
		
		// add a card to the array of cards this player has
		// param: itemtype, index of the card
		addCard: function(itemtype, card) {
			this.cards[itemtype].push(card);
			
			// mark card as my deck
			this.updateGuessTable(this.playerid, itemtype, card, 'X');
		},
		
		// called when the player is fully initialised
		ready: function() {
			// make sure my guesstable knows that I don't have any other cards
			// the check to not-overwrite the X with - happens in the updateGuessTable method
			for (var itemtype = 0; itemtype < this.items.length; ++itemtype) {
				for (var item = 0; item < this.items[itemtype].length; ++item) {
					this.updateGuessTable(this.playerid, itemtype, item, '-');
				}
			}
		},
		
		// internal function to add a new turn to the guess table, for the specified player
		addTurn: function(player) {
			for (var itemtype = 0; itemtype < this.items.length; ++itemtype) {
				for (var item = 0; item < this.items[itemtype].length; ++item) {
					// copy the X and - values of previous turns of this player
					var value = '0';
					if (this.guesstable[itemtype][item][player].length > 0) {
						var prevvalue = this.guesstable[itemtype][item][player][this.guesstable[itemtype][item][player].length - 1];
						if (prevvalue == 'X' || prevvalue == '-') {
							value = prevvalue;
						}
					}
					this.guesstable[itemtype][item][player].push(value);
				}
			}
		},
		
		// internal function to update the guess table with new information
		updateGuessTable: function(player, itemtype, card, newvalue) {
			var lastturnindex = this.guesstable[itemtype][card][player].length - 1;
			var oldval = this.guesstable[itemtype][card][player][lastturnindex];
			
			// update for the player in parameter
			if (oldval == '0' || oldval == '?') {
				this.guesstable[itemtype][card][player][lastturnindex] = newvalue;
			}
			// once a value of '-' or 'X' has been given, it cannot be changed
			
			// note: this table requires sweeping, use the internal sweepGuessTable function after all adding is done!
		},
		
		// internal
		sweepGuessTable: function() {
			// update what I know about other players
			
			/*
			- zolang er iets aangepast geweest is aan tabel:
			  - voor elk itemtype, item:
			    - voor elke player, kijk in zijn laatste turn of er een 'X' of '-' staat
			      - zo ja:
			        - distribueer die 'X' of '-' naar al zijn vorige turns
			        - als dit een 'X' is: in de vorige turns, zet alle eventuele '?' in de turn op '0' (omdat we anders in volgende stap, de '?'-sweep, fouten zullen maken)
			        - distribueer een '-' naar alle turns van alle andere spelers als deze een 'X' heeft
			
			  - als de vorige stap iets veranderd heeft aan de tabel:
			  - voor elk itemtype, item:
			    - voor elke player, kijk per turn
			      - staat in de turn precies 1 '?' en verder geen 'X'?
			        - zo ja:
			          - (verander de '?' in een 'X') (hoeft niet, gebeurt in stap 1 van sweep nadat we volgende instructie doen)
			          - zet een 'X' op de laatste turn van deze player
			*/
			
			var changed = true;
			while (changed) {
				changed = false;
				
				// step 1: find an 'X' in the last turn of each player
				for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
					for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
						for (var pl = 0; pl < this.players.length; ++pl) {
							var lastturn = this.guesstable[itemtype][item][pl].length - 1;
							if (lastturn >= 0 && (this.guesstable[itemtype][item][pl][lastturn] == 'X' || this.guesstable[itemtype][item][pl][lastturn] == '-')) {
								// 'X' or '-' found in last turn, distribute to previous turns
								var value = this.guesstable[itemtype][item][pl][lastturn];
								for (var lt = lastturn - 1; lt >= 0; --lt) {
									if (this.guesstable[itemtype][item][pl][lt] != value) {
										this.guesstable[itemtype][item][pl][lt] = value;
										changed = true;
										
										// change a '?' in the other fields of this turn to '0'
										if (value == 'X') {
											for (var turnitemtype = 0; turnitemtype < this.guesstable.length; ++turnitemtype) {
												for (var turnitem = 0; turnitem < this.guesstable[turnitemtype].length; ++turnitem) {
													if (this.guesstable[turnitemtype][turnitem][pl][lt] == '?') {
														this.guesstable[turnitemtype][turnitem][pl][lt] = '0';
													}
												}
											}
										}
									}
								}
								
								if (value == 'X') {
									// distribute to other players (a '-') if current player has 'X' (if he has the card, the others can't)
									for (var opl = 0; opl < this.players.length; ++opl) {
										if (opl != pl) {
											for (var t = 0; t < this.guesstable[itemtype][item][opl].length; ++t) {
												if (this.guesstable[itemtype][item][opl][t] != '-') {
													this.guesstable[itemtype][item][opl][t] = '-';
													changed = true;
												}
											}
										}
									}
								}
							}
						}
					}
				}
				
				// step 2
				if (changed) {
					for (var pl = 0; pl < this.players.length; ++pl) {
						var counter = 0;
						var target_itemtype = 0;
						var target_item = 0;
						var bignono = false;
						for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
							for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
								for (var t = 0; t < this.guesstable[itemtype][item][pl].length; ++t) {
									if (this.guesstable[itemtype][item][pl][t] == '?') {
										counter++;
										target_itemtype = itemtype;
										target_item = item;
									}
									else if (this.guesstable[itemtype][item][pl][t] == 'X') {
										// if there is an X in the column, it has precedence
										// wait what, how could there be an X and still a '?' here? Step 1 fixes that??
										bignono = true;
									}
								}
							}
						}
						if (counter == 1 && !bignono) {
							// set the lastturn value to 'X', step 1 will distribute it further
							var lastturn = this.guesstable[target_itemtype][target_item][pl].length - 1;
							this.guesstable[target_itemtype][target_item][pl][lastturn] = 'X';
							changed = true;
						}
					}
				}
			}
			
		},
		
		// generate a new suspect to ask the table
		// output: [index of card, index of card, index of card]
		makeGuess: function() {
			var ret = [];
			
			// todo: implement this for this player type!!
			
			
			
			// for each itemtype, find the card with the least known information:
			// see if there is an accusation (all '-'), pick that if you can (see mental note at the bottom)
			// count the card with the most '0'
			// if not found: count the one with the most '?'
			// if not found: is this possible?
			// mental note: in real life, you shouldn't pick the accusation card, as that could give away hints. You should pick the acc card, your own card if you have one for that itemtype, and some card you know the last player in the sequence will have, so you have the most chance of finding out the card you really want to know about
			
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				var mincard = -1;
				var mincount = 0;
				
				// find "accusation"
				for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
					var cardcount = 0;
					for (var pl = 0; pl < this.players.length; ++pl) {
						if (this.guesstable[itemtype][item][pl].length > 0 && this.guesstable[itemtype][item][pl][0] == '-') {
							// hardcoded 0 is allowed because of sweep
							cardcount++;
						}
					}
					if (cardcount == this.players.length) {
						mincard = item;
					}
				}
				
				if (mincard == -1) {
					// find "most '?'"
					mincount = 0; // reset
					for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
						var cardcount = 0;
						for (var pl = 0; pl < this.players.length; ++pl) {
							for (var t = 0; t < this.guesstable[itemtype][item][pl].length; ++t) {
								if (this.guesstable[itemtype][item][pl][t] == '?') {
									cardcount++;
								}
							}
						}
						if (cardcount > mincount) { // if two cards have the same count, the first one in the search order will be picked. That's okay!
							mincount = cardcount;
							mincard = item;
						}
					}
				}
				
				if (mincard == -1) {
					// find "most '0'"
					for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
						mincount = 0; // reset
						var cardcount = 0;
						for (var pl = 0; pl < this.players.length; ++pl) {
							for (var t = 0; t < this.guesstable[itemtype][item][pl].length; ++t) {
								if (this.guesstable[itemtype][item][pl][t] == '0') {
									cardcount++;
								}
							}
						}
						if (cardcount > mincount) { // if two cards have the same count, the first one in the search order will be picked. That's okay!
							mincount = cardcount;
							mincard = item;
						}
					}
				}
				
				if (mincard == -1) {
					// todo: find what this player knows least about (random '-' in this itemtype)
					for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
						var lastturn = this.guesstable[itemtype][item][this.playerid].length - 1;
						if (this.guesstable[itemtype][item][this.playerid][t] == '-') {
							mincard = item;
							break;
						}
					}
				}
				if (mincard == -1) {
					// is this possible?
					alert('boing');
				}
				
				ret.push(mincard);
			}
			
			return ret;
		},
		
		// generate an accusation to try to attempt and win the game. return [] when not at this time
		makeAccusation: function() {
			var ret = [];
			var canmakeaccusation = true;
			
			// for each itemtype, see which card nobody has (all '-')
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				var mincount = 0;
				var mincard = -1;
				for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
					var cardcount = 0;
					for (var pl = 0; pl < this.players.length; ++pl) {
						if (this.guesstable[itemtype][item][pl].length > 0 && this.guesstable[itemtype][item][pl][0] == '-') {
							// hardcoded 0 is allowed, because of sweep
							cardcount++;
						}
					}
					if (cardcount > mincount) {
						mincount = cardcount;
						mincard = item;
					}
				}
				if (mincount == this.players.length) {
					ret.push(mincard);
				}
				else {
					canmakeaccusation = false;
				}
			}
			
			if (!canmakeaccusation) {
				// clear ret again if we know it's an invalid accusation
				ret = []
			}
			
			return ret;
		},
		
		// check if you have a suspected item in my list
		// parameter: [ index of the card the player wants to check, index of another card, index of another card ]
		// return which card you want to show the player if you have at least one
		// return: [ null, index of the card i want to show, null ] (null = card type i don't want to show/have)
		checkGuess: function(guess) {
			var ret = [];
			
			// look in all my cards if i have one or more of the requested cards
			for (var itemtype = 0; itemtype < this.cards.length; ++itemtype) {
				// i can have several cards of the same itemtype
				for (var mycard = 0; mycard < this.cards[itemtype].length; ++mycard) {
					if (this.cards[itemtype][mycard] == guess[itemtype]) {
						// we have this card, now create a mini array with all types, and add the index of this card at the right spot
						var minilist = [];
						for (var it = 0; it < this.cards.length; ++it) {
							if (it == itemtype) {
								minilist[it] = guess[itemtype];
							}
							else {
								minilist[it] = null;
							}
						}
						
						ret.push(minilist);
					}
				}
			}
			// return a random card from the set of found cards
			if (ret.length > 0) {
				return ret[Math.floor(Math.random() * ret.length)];
			}
			else {
				return [];
			}
		},
		
		// function called when a player reacts to the suspect proposed by a player who is not me
		updatePlayerHasOne: function (askerplayer, playerwhohas, move) {
			// create a turn to store info in
			this.addTurn(playerwhohas);
			
			for (var itemtype = 0; itemtype < move.length; ++itemtype) {
				this.updateGuessTable(playerwhohas, itemtype, move[itemtype], '?');
			}
			
			this.sweepGuessTable();
		},
		
		// function called when a player does not react to a suspect proposed by a player (who could be me)
		updatePlayerHasNot: function (askerplayer, playerwhohasnot, move) {
			if (playerwhohasnot != this.playerid) { // I already know I don't have it, no need to store it in a turn
				// create a turn to store info in
				this.addTurn(playerwhohasnot);
				
				for (var itemtype = 0; itemtype < move.length; ++itemtype) {
					this.updateGuessTable(playerwhohasnot, itemtype, move[itemtype], '-');
				}
				
				this.sweepGuessTable();
			}
		},
		
		// function called when a player reacts to a suspect proposed by me
		updatePlayerHas: function (player, answer) {
			// create a turn to store info in
			this.addTurn(player);
			
			for (var itemtype = 0; itemtype < answer.length; ++itemtype) {
				if (answer[itemtype] != null) {
					this.updateGuessTable(player, itemtype, answer[itemtype], 'X');
				}
			}
			
			this.sweepGuessTable();
		},
		
		printGuessTable: function() {
			var tempacc = [];
			for (var itemtype = 0; itemtype < this.guesstable.length; ++itemtype) {
				tempacc.push(itemtype + ":");
				for (var item = 0; item < this.guesstable[itemtype].length; ++item) {
					var playerinfo = '';
					var totalvalue = '0';
					for (var pl = 0; pl < this.players.length; ++pl) {
						playerinfo += 'pl_' + pl + ':' + this.guesstable[itemtype][item][pl] + ': ';
						var lastvalue = this.guesstable[itemtype][item][pl][this.guesstable[itemtype][item][pl].length - 1];
						if (lastvalue == '-') {
							if (totalvalue == '0' || totalvalue == '?') {
								totalvalue = lastvalue;
							}
						}
						else if (lastvalue == 'X') {
							if (totalvalue == '0' || totalvalue == '?' || totalvalue == '-') {
								totalvalue = lastvalue;
							}
						}
					}
					tempacc.push(item + '> ' + playerinfo + ' > ' + totalvalue);
				}
			}
			return tempacc;
		}
	};
}

